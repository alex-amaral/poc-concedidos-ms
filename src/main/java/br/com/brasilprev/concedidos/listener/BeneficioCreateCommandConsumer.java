package br.com.brasilprev.concedidos.listener;

import br.com.brasilprev.concedidos.domain.BeneficioRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.messaging.handler.annotation.Payload;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

@Slf4j
@AllArgsConstructor
//@EnableBinding(Sink.class)
public class BeneficioCreateCommandConsumer {

    private BeneficioRepository beneficioRepository;


    @StreamListener(target = Sink.INPUT)
    public void consume(String message) {

        log.info("recieved a string message : " + message);
    }

    @StreamListener(target = Sink.INPUT, condition = "headers['type']=='Bchat'")
    public void handle(@Payload ChatMessage message) {

        final DateTimeFormatter df = DateTimeFormatter.ofLocalizedTime(FormatStyle.MEDIUM)
                .withZone(ZoneId.systemDefault());
        final String time = df.format(Instant.ofEpochMilli(message.getTime()));
        log.info("recieved a complex message : [{}]: {}", time, message.getContents());
    }
}
