package br.com.brasilprev.concedidos.domain;

import io.eventuate.Event;
import io.eventuate.ReflectiveMutableCommandProcessingAggregate;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

import static io.eventuate.EventUtil.events;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Solicitacao extends ReflectiveMutableCommandProcessingAggregate<Solicitacao, SolicitacaoCommand> {

    private SolicitacaoState estado;
    private String matricula;

    public List<Event> process(CreateSolicitacaoCommand cmd) {
        return events(new SolicitacaoCreatedEvent(cmd.getMatricula()));
    }

    public void apply(SolicitacaoCreatedEvent event) {
        this.estado = SolicitacaoState.CRIADA;
        this.matricula= event.getMatricula();
    }
}
