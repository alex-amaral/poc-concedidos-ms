package br.com.brasilprev.concedidos.domain;

import io.eventuate.Event;
import io.eventuate.EventEntity;

@EventEntity(entity = "br.com.brasilprev.concedidos.domain.Solicitacao")
public interface SolicitacaoEvent extends Event {
}
