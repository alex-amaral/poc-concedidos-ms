package br.com.brasilprev.concedidos.domain;

public enum SolicitacaoState {
    CRIADA, VALIDADA, EM_DESFRUTE, FINALIZADA;
}
