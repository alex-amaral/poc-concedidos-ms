package br.com.brasilprev.concedidos.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class SolicitacaoCreatedEvent implements SolicitacaoEvent {

    private String matricula;
}

