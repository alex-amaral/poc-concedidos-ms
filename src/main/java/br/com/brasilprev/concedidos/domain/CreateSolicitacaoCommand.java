package br.com.brasilprev.concedidos.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class CreateSolicitacaoCommand implements SolicitacaoCommand {

    private String matricula;
}
