package br.com.brasilprev.concedidos.domain;

import io.eventuate.EntityWithIdAndVersion;
import io.eventuate.EventHandlerContext;
import io.eventuate.EventHandlerMethod;
import io.eventuate.EventSubscriber;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CompletableFuture;

@Slf4j
@EventSubscriber(id = "customerWorkflow")
public class SolicitacaoWorkflow {


    @EventHandlerMethod
    public CompletableFuture<EntityWithIdAndVersion<?>> reserveCredit(EventHandlerContext<SolicitacaoCreatedEvent> ctx) {
        SolicitacaoCreatedEvent event = ctx.getEvent();

        log.info("Recebido o evento de Solicitacao já criada");

        return null;
    }
}
