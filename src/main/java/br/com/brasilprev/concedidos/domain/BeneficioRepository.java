package br.com.brasilprev.concedidos.domain;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface BeneficioRepository extends CrudRepository<Beneficio,Long> {


    public Optional<Beneficio> findByMatricula(String matricula);

}
