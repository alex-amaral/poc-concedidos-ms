package br.com.brasilprev.concedidos.domain;


import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.math.BigDecimal;


@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Beneficio extends AbstractPersistable<Long> {

    private String matricula;
    private String beneficiario;
    @Enumerated(EnumType.STRING)
    private TipoRenda tipoRenda;
    private BigDecimal valor;
    private Integer diaPagamento;

}
