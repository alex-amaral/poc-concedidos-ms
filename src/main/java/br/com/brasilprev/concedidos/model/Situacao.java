package br.com.brasilprev.concedidos.model;

public enum Situacao {
    LIBERADO, EM_ANALISE, NEGADO
}
