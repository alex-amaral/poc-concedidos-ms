package br.com.brasilprev.concedidos.api.desacumulo;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class DesacumuloDto {

    private String matricula;
    private List<BeneficiarioDto> beneficiarios;
}
