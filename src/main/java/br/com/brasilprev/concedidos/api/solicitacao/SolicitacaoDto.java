package br.com.brasilprev.concedidos.api.solicitacao;

import br.com.brasilprev.concedidos.domain.Beneficio;
import br.com.brasilprev.concedidos.domain.TipoRenda;
import br.com.brasilprev.concedidos.model.Situacao;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class SolicitacaoDto implements Serializable {

    private String matricula;
    private String beneficiario;
    private Situacao situacao;
    private BigDecimal valor;

    public Beneficio toBeneficio() {

        return new Beneficio(matricula,beneficiario, TipoRenda.TRADICIONAL, valor, 10);
    }
}
