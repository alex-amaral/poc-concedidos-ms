package br.com.brasilprev.concedidos.api.solicitacao;

import br.com.brasilprev.concedidos.domain.CreateSolicitacaoCommand;
import br.com.brasilprev.concedidos.domain.Solicitacao;
import jdk.nashorn.internal.objects.annotations.Getter;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

import static br.com.brasilprev.concedidos.model.Situacao.*;
import static java.math.BigDecimal.valueOf;

@RestController
public class SolicitacaoResource {

    @GetMapping("solicitacao")
    public ResponseEntity<?> consultaGeral(){
        final List<SolicitacaoDto> solicitacoes = Arrays.asList(
            new SolicitacaoDto("100000-001", "Jose das Couves", LIBERADO, valueOf(50000)),
            new SolicitacaoDto("500000-000", "Maria Antonieta", EM_ANALISE,  valueOf(70000)),
            new SolicitacaoDto("680000-010", "Josefina de Almeida", NEGADO, valueOf(1000000)),
            new SolicitacaoDto("200000-001", "Octavia Franca", LIBERADO, valueOf(50000)),
            new SolicitacaoDto("300000-000", "Oswaldo de Souza", EM_ANALISE,  valueOf(70000)),
            new SolicitacaoDto("480000-010", "Carlos de Almeida", NEGADO, valueOf(1000000)),
            new SolicitacaoDto("500000-001", "Jose Pedro das Couves", LIBERADO, valueOf(50000)),
            new SolicitacaoDto("650000-000", "Helena Pereira Amaral", EM_ANALISE,  valueOf(70000)),
            new SolicitacaoDto("780000-010", "Cristina Pereira", NEGADO, valueOf(1000000)),
            new SolicitacaoDto("800000-001", "Zilda das Couves", LIBERADO, valueOf(50000)),
            new SolicitacaoDto("900000-000", "Antonieta de Castro", EM_ANALISE,  valueOf(70000)),
            new SolicitacaoDto("100000-010", "Jiselda Carla Almeida", NEGADO, valueOf(1000000)),
            new SolicitacaoDto("110000-001", "Pedro Jose Mediros", LIBERADO, valueOf(50000)),
            new SolicitacaoDto("120000-000", "Ana Maria  Antonieta", EM_ANALISE,  valueOf(70000)),
            new SolicitacaoDto("130000-010", "Pedro algusto de Almeida", NEGADO, valueOf(1000000)),
            new SolicitacaoDto("150000-001", "Antonio das Couves", LIBERADO, valueOf(50000)),
            new SolicitacaoDto("560000-000", "Maria Joaquina Santos", EM_ANALISE,  valueOf(70000)),
            new SolicitacaoDto("690000-010", "Jesus  dos Santos", NEGADO, valueOf(1000000)),
            new SolicitacaoDto("166000-001", "Marco Antonio das Couves", LIBERADO, valueOf(50000)),
            new SolicitacaoDto("598660-000", "Flor Antonieta", EM_ANALISE,  valueOf(70000)),
            new SolicitacaoDto("680001-010", "Adelia de Almeida", NEGADO, valueOf(1000000))
        );
        
       return ResponseEntity.ok(solicitacoes);
    }

    @GetMapping("api/solicitacao/create")
    public ResponseEntity<?> createSolicitacao(){

        Solicitacao solicitacao = new Solicitacao();
        solicitacao.process( new CreateSolicitacaoCommand("0001"));

        return ResponseEntity.ok("OK");
    }
}
