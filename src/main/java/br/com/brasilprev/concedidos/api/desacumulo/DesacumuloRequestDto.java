package br.com.brasilprev.concedidos.api.desacumulo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class DesacumuloRequestDto {

    private String matricula;
    private String beneficiario;
    private BigDecimal valor;

}
