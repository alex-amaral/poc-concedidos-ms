package br.com.brasilprev.concedidos.api;

import br.com.brasilprev.concedidos.api.solicitacao.SolicitacaoDto;
import br.com.brasilprev.concedidos.domain.Beneficio;
import br.com.brasilprev.concedidos.domain.BeneficioRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@Slf4j
@RestController
@AllArgsConstructor
public class BeneficioResource {

    private BeneficioRepository beneficioRepository;

    @PostMapping("api/beneficio")
    public ResponseEntity<?> create(@RequestBody BeneficioDto beneficioDto){

        Optional<Beneficio> beneficio = beneficioRepository.findByMatricula(beneficioDto.getMatricula());

        if (beneficio.isPresent()){
            log.warn("Beneficio já cadastrado.");
            return ResponseEntity.badRequest()
                    .body("Beneficio com matricula "+ beneficioDto.getMatricula() +" ja cadastrada" );
        }
        return ResponseEntity.ok(
                beneficioRepository.save(beneficioDto.toBeneficio()));
    }
}