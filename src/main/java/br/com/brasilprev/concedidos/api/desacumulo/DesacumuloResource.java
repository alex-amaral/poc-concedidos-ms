package br.com.brasilprev.concedidos.api.desacumulo;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Arrays;

@RestController
public class DesacumuloResource {

    @PostMapping("desacumulo")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> create(@RequestBody final DesacumuloRequestDto desacumuloRequest){

        return ResponseEntity.ok(new DesacumuloDto(desacumuloRequest.getMatricula(), Arrays.asList(
                new BeneficiarioDto(desacumuloRequest.getBeneficiario(), desacumuloRequest.getValor()))));
    }

    @GetMapping("desacumulo/{matricula}")
    public ResponseEntity<?> query(@PathVariable( required = true) final String matricula){

        final DesacumuloDto desacumuloDto = new DesacumuloDto(matricula, Arrays.asList(
                        new BeneficiarioDto("Jose das couves", BigDecimal.valueOf(50000)),
                        new BeneficiarioDto("Maria das couves", BigDecimal.valueOf(60000))));

        return ResponseEntity.ok(desacumuloDto);
    }

    @PutMapping("desacumulo/{matricula}")
    public ResponseEntity<?> update(@PathVariable( required = true) final String matricula){

        return ResponseEntity.ok(new DesacumuloDto(matricula,
                Arrays.asList(new BeneficiarioDto("Jose das couves", BigDecimal.valueOf(50000)))));
    }

    @DeleteMapping("desacumulo/{matricula}")
    public void delete(@PathVariable( required = true) final String matricula){

        // chama o fluxo de criação
    }
}
